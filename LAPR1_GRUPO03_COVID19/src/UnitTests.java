import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Arrays;

public class UnitTests {

    public static void main(String[] args) throws IOException, ParseException {
        runTests();


    }

    private static boolean test_countDays(LocalDate startDate, LocalDate endDate, int expectedResult) {
        boolean flag = false;

        if (Main.countDays(startDate, endDate) == expectedResult) {
            flag = true;
        }

        return flag;
    }


    private static boolean test_getDailyData(String[][] data, int days, String[] parameters, String[][] expectedResult) {
        boolean flag = false;
        if (Arrays.deepEquals(Main.getDailyData(data, days, parameters), expectedResult)) {
            flag = true;
        }
        return flag;
    }

    private static String[][] data() {
        String[][] dataToReturn = new String[2][6];
        dataToReturn[0][0] = "2020-04-01";
        dataToReturn[0][1] = "10170223";
        dataToReturn[0][2] = "8251";
        dataToReturn[0][3] = "5664";
        dataToReturn[0][4] = "1293";
        dataToReturn[0][5] = "187";
        dataToReturn[1][0] = "2020-04-02";
        dataToReturn[1][1] = "10170850";
        dataToReturn[1][2] = "9034";
        dataToReturn[1][3] = "6706";
        dataToReturn[1][4] = "1533";
        dataToReturn[1][5] = "209";
        return dataToReturn;
    }

    private static String[] parameters() {

        String[] dataToReturn = {"0", "0", "0", "0", "1", "1"};
        return dataToReturn;
    }

    private static String[][] expectedResult() {
        String[][] dataToReturn = {
                {"Data", "Infectado", "Hospitalizado", "Internados UCI", "Mortes"},
                {"2020-04-02", "783", "1042", "240", "22"}
        };

        return dataToReturn;
    }

    private static boolean test_getWeeklyData(String[][] data, int days, String[] parameters, String[][] expectedResult) throws IOException, ParseException {
        boolean flag = false;
        if (Arrays.deepEquals(Main.getWeeklyData(data, days, parameters), expectedResult)) {
            flag = true;
        }
        return flag;
    }

    private static String[][] dataWeekTeste1() {
        String[][] dataToReturn = {
                {"2020-04-04", "2", "2", "2", "2", "2"},
                {"2020-04-05", "4", "4", "4", "4", "4"},
                {"2020-04-06", "6", "6", "6", "6", "6"},
                {"2020-04-07", "8", "8", "8", "8", "8"},
                {"2020-04-08", "10", "10", "10", "10", "10"},
                {"2020-04-09", "12", "12", "12", "12", "12"},
                {"2020-04-10", "14", "14", "14", "14", "14"},
                {"2020-04-11", "16", "16", "16", "16", "16"},
                {"2020-04-12", "18", "18", "18", "18", "18"}
        };
        return dataToReturn;
    }

    private static String[][] expectedResultWeeklyTeste1() {
        String[][] dataToReturn = {
                {"Semana ", "Infectado", "Hospitalizado", "Internados UCI", "Mortes"},
                {"Semana 1", "14", "14", "14", "14"}
        };
        return dataToReturn;
    }

    private static String[][] dataWeekTeste2() {
        String[][] dataToReturn = {
                {"2020-04-04", "0", "0", "0", "0", "0"},
                {"2020-04-05", "4", "4", "4", "4", "4"},
                {"2020-04-06", "8", "8", "8", "8", "8"},
                {"2020-04-07", "12", "12", "12", "12", "12"},
                {"2020-04-08", "16", "16", "16", "16", "16"},
                {"2020-04-09", "20", "20", "20", "20", "20"},
                {"2020-04-10", "24", "24", "24", "24", "24"},
                {"2020-04-11", "28", "28", "28", "28", "28"},
                {"2020-04-12", "32", "32", "32", "32", "32"}
        };
        return dataToReturn;
    }

    private static String[][] expectedResultWeeklyTeste2() {
        String[][] dataToReturn = {
                {"Semana ", "Infectado", "Hospitalizado", "Internados UCI", "Mortes"},
                {"Semana 1", "28", "28", "28", "28"}
        };
        return dataToReturn;
    }

    private static boolean test_getMonthlyData(String[][] data, int days, String[] parameters, String[][] expectedResult) throws IOException, ParseException {
        boolean flag = false;
        if (Arrays.deepEquals(Main.getMonthlyData(data, days, parameters), expectedResult)) {
            flag = true;
        }
        return flag;
    }

    private static String[][] dataMonthlyTeste1() {
        String[][] dataToReturn = {
                {"2020-04-30", "2", "2", "2", "2", "2"},
                {"2020-05-01", "2", "2", "2", "2", "2"},
                {"2020-05-02", "4", "4", "4", "4", "4"},
                {"2020-05-03", "6", "6", "6", "6", "6"},
                {"2020-05-04", "8", "8", "8", "8", "8"},
                {"2020-05-05", "10", "10", "10", "10", "10"},
                {"2020-05-06", "12", "12", "12", "12", "12"},
                {"2020-05-7", "14", "14", "14", "14", "14"},
                {"2020-05-8", "16", "16", "16", "16", "16"},
                {"2020-05-9", "18", "18", "18", "18", "18"},
                {"2020-05-10", "18", "18", "18", "18", "18"},
                {"2020-05-11", "18", "18", "18", "18", "18"},
                {"2020-05-12", "18", "18", "18", "18", "18"},
                {"2020-05-13", "18", "18", "18", "18", "18"},
                {"2020-05-14", "18", "18", "18", "18", "18"},
                {"2020-05-15", "18", "18", "18", "18", "18"},
                {"2020-05-16", "18", "18", "18", "18", "18"},
                {"2020-05-17", "18", "18", "18", "18", "18"},
                {"2020-05-18", "18", "18", "18", "18", "18"},
                {"2020-05-19", "18", "18", "18", "18", "18"},
                {"2020-05-20", "18", "18", "18", "18", "18"},
                {"2020-05-21", "18", "18", "18", "18", "18"},
                {"2020-05-22", "18", "18", "18", "18", "18"},
                {"2020-05-23", "18", "18", "18", "18", "18"},
                {"2020-05-24", "18", "18", "18", "18", "18"},
                {"2020-05-25", "18", "18", "18", "18", "18"},
                {"2020-05-26", "18", "18", "18", "18", "18"},
                {"2020-05-27", "18", "18", "18", "18", "18"},
                {"2020-05-28", "18", "18", "18", "18", "18"},
                {"2020-05-29", "18", "18", "18", "18", "18"},
                {"2020-05-30", "18", "18", "18", "18", "18"},
                {"2020-05-31", "18", "18", "18", "18", "18"},
        };
        return dataToReturn;
    }

    private static String[][] expectedResultMonthlyTeste1() {
        String[][] dataToReturn = {
                {"Mês", "Infectado", "Hospitalizado", "Internados UCI", "Mortes"},
                {"Mês 1", "16", "16", "16", "16"}
        };
        return dataToReturn;
    }

    private static boolean test_matrixTransformationForComparation(String[][] data, int columnToShow, int days, String[][] expectedResult) {
        boolean flag = false;
        if (Arrays.deepEquals(Main.matrixTransformationForComparison(data, columnToShow, days), expectedResult)) {
            flag = true;
        }
        return flag;
    }

    private static String[][] dataForTestMatrix1() {
        String[][] dataToReturn = {
                {"01-01-2021", "1", "1", "1", "1", "1"},
                {"02-02-2021", "2", "2", "2", "2", "2"}
        };
        return dataToReturn;
    }

    private static String[][] expectedResultTestMatrixTransformation1() {
        String[][] dataToReturn = {
                {"Data", "Mortes"},
                {"02-02-2021", "2"}
        };
        return dataToReturn;
    }

    private static String[][] dataForTestMatrix2() {
        String[][] dataToReturn = {
                {"10-01-2021", "7", "7", "7", "7", "7"},
                {"11-01-2021", "5", "5", "5", "5", "5"}
        };
        return dataToReturn;
    }

    private static String[][] expectedResultTestMatrixTransformation2() {
        String[][] dataToReturn = {
                {"Data", "Infectado"},
                {"11-01-2021", "5"}
        };
        return dataToReturn;
    }

    private static boolean test_organizationMatrixToShow(String[][] data1, String[][] data2, int daysOrganization1, int daysOrganization2, String[][] expectedResult) {
        boolean flag = false;
        if (Arrays.deepEquals(Main.organizeMatrixToShow(data1, data2, daysOrganization1, daysOrganization2), expectedResult)) {
            flag = true;
        }
        return flag;
    }

    private static String[][] dataOrganization1() {
        String[][] result = {
                {"1", "2"},
                {"3", "4"}
        };
        return result;
    }

    private static String[][] dataOrganization2() {
        String[][] result = {
                {"5", "6"},
                {"8", "7"}
        };
        return result;
    }

    private static String[][] expectedResultOrganitazio() {
        String[][] result = {
                {"1", "2", "5", "6", "Diferença"},
                {"3", "4", "8", "7", null}
        };
        return result;
    }

    private static boolean test_comparisonMode(String[] comparationParameters, String[][] expectedResult) throws IOException, ParseException {
        boolean flag = false;
        if (Arrays.deepEquals(Main.comparisonMode(comparationParameters),expectedResult)){

            flag = true;
        }
        return flag;
    }

    private static String[] comparisonParameters() {
        String[] dataToReturn = {"01-10-2021", "02-10-2021", "04-10-2021", "06-10-2021", "1", "2", "covid19.csv"};
        return dataToReturn;
    }

    private static String[][] expectedResultForComparison() {
        String[][] dataToReturn = {
                {"Data", "Infectados", "Data", "Infectados", "Diferença"},
                {"2021-10-01", "696", "2021-10-04", "193", "503"},
                {"2021-10-02", "690", "2021-10-05", "730", "40"}
        };
        return dataToReturn;
    }

    private static boolean test_sumTotalWeek(String[][] data, int firstLine, int column, String expectedResult) {
        boolean flag = false;
        if (Main.sumTotalWeek(data, firstLine, column).equalsIgnoreCase(expectedResult)) {
            flag = true;
        }
        return flag;
    }

    private static String[][] dataForSumWeek() {
        String[][] dataToReturn = {
                {"10", "10", "10", "5"},
                {"12", "5", "15", "20"},
                {"5", "2", "10", "2"},
                {"10", "10", "5", "1"},
                {"10", "10", "10", "5"},
                {"12", "5", "15", "20"},
                {"5", "2", "10", "2"},
        };
        return dataToReturn;
    }

    /**
     * private static boolean test_callInteractiveMode(Integer escolha, int expectedResult) {
     * boolean flag = false;
     * if (Main.callInteractiveMode(null, escolha) == expectedResult) {
     * flag = true;
     * }
     * return flag;
     * }
     */

    public static void runTests() throws IOException, ParseException {
        System.out.println("conta dias:" + test_countDays(LocalDate.parse("2021-12-31"), LocalDate.parse("2022-01-01"), 1)
        );
        System.out.println("getDailyData-Teste: " + test_getDailyData(data(), 2, parameters(), expectedResult()));
        System.out.println("getWeeklyData-Teste1: " + test_getWeeklyData(dataWeekTeste1(), 9, parameters(), expectedResultWeeklyTeste1()));
        System.out.println("getWeeklyData-Teste2: " + test_getWeeklyData(dataWeekTeste2(), 9, parameters(), expectedResultWeeklyTeste2()));
        System.out.println("getMonthlyData-Teste: " + test_getMonthlyData(dataMonthlyTeste1(), 32, parameters(), expectedResultMonthlyTeste1()));
        System.out.println("matrixTransformationForComparation-Teste1: " + test_matrixTransformationForComparation(dataForTestMatrix1(), 5, 2, expectedResultTestMatrixTransformation1()));
        System.out.println("matrixTransformationForComparation-Teste2: " + test_matrixTransformationForComparation(dataForTestMatrix2(), 2, 2, expectedResultTestMatrixTransformation2()));
        System.out.println("OrganizationMatrix-Teste1: " + test_organizationMatrixToShow(dataOrganization1(), dataOrganization2(), 2, 2, expectedResultOrganitazio()));
        System.out.println("sumTotalWeek_teste: " + test_sumTotalWeek(dataForSumWeek(), 0, 1, "44"));
        System.out.println("sumTotalWeek_teste2: " + test_sumTotalWeek(dataForSumWeek(), 0, 2, "75"));

    }
}
