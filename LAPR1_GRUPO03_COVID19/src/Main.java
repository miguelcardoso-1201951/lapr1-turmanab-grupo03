import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

public class Main {

    static int COLUMNS = 6;
    static int sizeMatrix = 4;
    static Scanner reader;

    /**
     * @param args parameters é o vector que vai guardar toda a informação recebida
     *             por parâmetro
     *             parameters[0] = time, parameters[1] = data inicio, parameters[2]
     *             = data de fim,
     *             parameters[3] = ficheiro.csv
     *             parameters[4] = ficheiro.txt
     */
    public static void main(String[] args) {


        try {
            startProgram(args);

        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    /**
     * processa a informação do modo interactivo quando apenas se quer exibir dados
     * "normais"
     *
     * @param dataValidated argumentos inseridos pelo utilizador e validados
     */
    public static void processData(String[] dataValidated) {

        try {
            String fileName = dataValidated[3];

            File file = new File(fileName);

            String startDateString = dataValidated[1];
            String endDateString = dataValidated[2];
            int typeOfFile = Integer.parseInt(dataValidated[4]);
            LocalDate startDate = invertDateFormat(startDateString);
            LocalDate endDate = invertDateFormat(endDateString);
            switch (typeOfFile) {
                case 1:
                    startDateString = String.valueOf(startDate);
                    endDateString = String.valueOf(endDate);
                    break;
                case 2:
                    startDateString = startDateString;
                    break;

                default:
                    System.out.println("-");
                    System.out.println("Opção inválida.");
                    System.out.println("-");
            }

            if (validateDateInFile(startDateString, file) && validateDateInFile(endDateString, file)) {
                int days = countDays(startDate, endDate) + 2;
                //coloquei +2 para conseguir ter desde o dia de inicio e dia anterior

                String[][] data = readData(file, days, startDate, typeOfFile);

                String[][] dataToShow = chooseTimeFrame(data, dataValidated, days);
                System.out.println("O intervalo de datas escolhido foi de " + dataValidated[1] + " até " + dataValidated[2] + ".");
                System.out.println();
                showOrSave(dataToShow);

            } else {

                System.out.println("-");
                System.out.println("A data de início especificada não se encontra no intervalo de datas do ficheiro fornecido.");
                System.out.println("Por favor escolha nova data ou novo ficheiro.");
                System.out.println("-");

                pressEnterToContinue();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    /**
     * @param data
     */
    public static void showOrSave(String[][] data) {
        int input;

        try {
            showData(data);

            do {
                reader = new Scanner(System.in);

                System.out.println("--------------------------");
                System.out.println("(1) Gravar em ficheiro CSV");
                System.out.println("(2) Sair do Programa      ");
                System.out.println("--------------------------");
                System.out.print("Indique a sua opção: ");

                input = reader.nextInt();

                switch (input) {
                    case 1:
                        System.out.println("");
                        System.out.println("Por favor indique o nome do ficheiro csv.");
                        System.out.println("");
                        String csvName = reader.next();
                        interactiveSaveToCsv(data, csvName);
                        System.exit(0);
                        break;
                    case 2:
                        System.exit(0);
                        break;
                    default:
                        System.out.println("-");
                        System.out.println("Opção inválida!");
                        System.out.println("-");
                        break;
                }

            } while (input != 2);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Método que grava array de Strings em ficheiro csv
     *
     * @param data    Matriz que deverá conter os dados a serem apresentados no csv
     * @param csvName nome do ficheiro csv, passado pelo utilizador
     */
    public static void interactiveSaveToCsv(String[][] data, String csvName) {
        try {
            //  File userFile = new File("LAPR1_GRUPO03_COVID19/files/" + csvName);
            File userFile = new File(csvName);
            if (userFile.createNewFile()) {
                System.out.println("");
                System.out.println("Foi criado um ficheiro com o nome " + userFile.getName());
                System.out.println("");
                FileWriter writeUserCsv = new FileWriter(userFile);
                for (int i = 0; i < data.length; i++) {
                    writeUserCsv.append(String.join(",", data[i][0]));
                    for (int j = 1; j < data[i].length; j++) {
                        writeUserCsv.append(",");
                        writeUserCsv.append(String.join(",", data[i][j]));
                    }
                    writeUserCsv.append("\n");
                }
                writeUserCsv.flush();
                writeUserCsv.close();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * exibe a matriz na consola
     *
     * @param data matriz a mostrar na consola
     */
    public static void showData(String[][] data) {

        try {
            for (int i = 0; i < data.length; i++) {
                System.out.printf("%-15s", data[i][0]);
                for (int j = 1; j < data[i].length; j++) {
                    System.out.printf("%-15s ", data[i][j]);
                }
                System.out.println();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    /**
     * validatedParameters:
     * [0] = resolução -> 0 = diária ; 1 = semanal ; 2 = mensal
     * [1] = data de inicio
     * [2] = data de fim
     * [3] = nome do ficheiro
     * [4] = tipo de ficheiro (acumulados/totais) -> 0 = nada ; 1 = acumulados ; 2 =
     * totais
     * [5] = dados a tratar -> 1 = todas; 2 = infectados; 3 = hospitalizados; 4 =
     * internados UCI; 5 = mortos
     * [6] = nome do ficheiro de Markov
     *
     * @param args argumentos inseridos pelo utilizador
     */
    public static void startProgram(String[] args) {
        try {
            // String[] validatedParameters = new String[5];
            String[] validatedParameters = new String[7];
            validatedParameters[6] = "-";
            // String[] validatedManualParameters = new String[12];

            try {
                if (args.length == 0) {
                    callInteractiveMode(validatedParameters);
                } else {
                    callManualMode(args);
                }
            } catch (Exception exception) {
                System.out.println("-");
                System.out.println("Aconteceu um erro de execução, por favor verifique os dados e volte a tentar.");
                System.out.println("-");
                exception.printStackTrace();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Método que trata o modo manual, ou modo não interativo
     *
     * @param args parâmetros recebidos do utilizador
     * @return parâmetros recebidos, validados
     */
    public static String[] callManualMode(String[] args) {
        // conta numero de argumentos para saber para onde encaminhar
        String[] receivedParameters = new String[args.length];

        try {
            if (receivedParameters.length == 20) {
                callCompleteManualMode(args);
                // entra no modo completo com previsoes
                System.out.println("-");
                System.out.println("Foi criado um ficheiro txt completo com o nome " + args[19] + ".");
                System.out.println("-");
            } else if (receivedParameters.length == 16) {
                callManualModeComparison(args);
                // entra no modo sem previsoes
                System.out.println("-");
                System.out.println("Foi criado um ficheiro txt completo sem previsões com o nome " + args[15] + ".");
                System.out.println("-");
            } else if (receivedParameters.length == 5) {
                callManualModePredictions(args);
                System.out.println("-");
                System.out.println("Foi criado um ficheiro txt apenas de previsões com o nome " + args[4] + ".");
                System.out.println("-");
                // entra no modo de apenas previsōes
            } else {
                System.out.println("-");
                System.out.println("Argumentos inseridos não estão previstos para a realização de estudos.");
                System.out.println("-");
                System.exit(0);
            }

            for (int i = 0; i < receivedParameters.length; i++) {
                receivedParameters[i] = args[i];
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return receivedParameters;
    }

    /**
     * este metodo faz uma verificação com as datas e posiçoes dos parametros
     * introduzidos
     *
     * @param args argumentos introduzidos pelo utilizador
     */

    public static void callManualModeComparison(String[] args) {

        try {
            // verifica se os dados estao todos correctos
            errorVerification(args);

            File fileDataAcumulated = new File(args[14]);

            // identificação do tipo de ficheiro para fazer a leitura e os dias que precisamos antes da data escolhida
            int acumulated = 1;
            int dayAndDayBefore = 2;

            // dias dentro do intervalo de datas
            int daysOnInterval = countDays(invertDateFormat(args[3]), invertDateFormat(args[5])) + dayAndDayBefore;

            // parametros necessarios para modo comparação
            String[] parametersForComparationAcumulated = {args[7], args[9], args[11], args[13], "1", "1", args[14]};

            // dados a ser trabalhados
            String[][] dataAcumulated = readData(fileDataAcumulated, daysOnInterval, invertDateFormat(args[3]),
                    acumulated);
            String[] parametersToGetAcumulated = {"0", "0", "0", "0", "1", "1"};

            // aprentação no tipo de resolucção escolhida
            if (args[1].equalsIgnoreCase("0")) {
                manualSaveDataToTxt(getDailyData(dataAcumulated, daysOnInterval, parametersToGetAcumulated), args[15],
                        "Dados diários em formato de novos casos. ");
            } else if (args[1].equalsIgnoreCase("1")) {
                manualSaveDataToTxt(getWeeklyData(dataAcumulated, daysOnInterval, parametersToGetAcumulated), args[15],
                        "Dados semanais em formato de novos casos.");

            } else {
                manualSaveDataToTxt(getMonthlyData(dataAcumulated, daysOnInterval, parametersToGetAcumulated), args[15],
                        "Dados mensais em formato de novos casos.");
            }
            // loop para apresentar as comparaçōes para todas as hipoteses (infectados,
            // internados, hospitalizados e mortes)
            for (int i = 1; i < 6; i++) {
                if (i + 1 < 6) {
                    parametersForComparationAcumulated[5] = Integer.toString(i + 1);
                    // os parametros não atualizam dentro do for e por isso tem de estar sempre +1 a
                    // frente
                    manualSaveDataToTxt(comparisonMode(parametersForComparationAcumulated), args[15],
                            "Comparação de novos casos diários.");
                    String[][] avgAndStdDeviation = calculateStdDeviation(
                            comparisonMode(parametersForComparationAcumulated),
                            calculateAverage(comparisonMode(parametersForComparationAcumulated)));
                    manualSaveDataToTxt(avgAndStdDeviation, args[15], "");
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * modo de apresentacao de resultados completo,
     * apresenta toda a informacao disponivel na aplicação percorrendo todos os
     * metodos.
     *
     * @param args sao parametros introduzidos pelo utilizador
     */
    public static void callCompleteManualMode(String[] args) {
        try {
            errorVerification(args);

            File fileDataTotal = new File(args[16]);
            File fileDataAcumulated = new File(args[17]);

            int acumulated = 1; // valor utilizado para identificar o tipo de ficheiro no readData
            int total = 2; // valor utilizado para identificar o tipo de ficheiro no readData
            int dayAndDayBefore = 2; // com este mais 2 conseguimos ir buscar mais 2 dias, o anterior e o dia exacto

            int daysFirstInterval = countDays(invertDateFormat(args[3]), invertDateFormat(args[5])) + dayAndDayBefore;
            // verifica numero de dias entre intervalo

            String[][] avgAndStdDeviation = new String[2][5];

            // string de parametros para enviar para o metodo getDaily, getWeekly, getMonthly

            String[] parametersForComparisonAcumulated = {args[7], args[9], args[11], args[13], "1", "1", args[17]};
            String[] parametersForComparisonTotal = {args[7], args[9], args[11], args[13], "2", "1", args[16]};

            // guarda as strings com as matrizes que necessitamos para trabalhar, acumulada e total
            String[][] dataTotal = readData(fileDataTotal, daysFirstInterval, invertDateFormat(args[3]), total);
            String[][] dataAcumulated = readData(fileDataAcumulated, daysFirstInterval, invertDateFormat(args[3]),
                    acumulated);

            // parametros para utilizar dentro dos metodos getDaily, getWeekly e getMonthly,
            // são inicializados aqui e alterados dentro do ciclo for
            String[] parametersToGetAcumulated = {"0", "0", "0", "0", "1", "1"};
            String[] parametersToGetTotal = {"0", "0", "0", "0", "2", "1"};

            // dependendo da escolha do utilizador mostra o resultado correcto
            if (args[1].equalsIgnoreCase("0")) {
                manualSaveDataToTxt(getDailyData(dataAcumulated, daysFirstInterval, parametersToGetAcumulated),
                        args[19], "Dados diários em formato de novos casos. ");
                manualSaveDataToTxt(getDailyData(dataTotal, daysFirstInterval, parametersToGetTotal), args[19],
                        "Dados diários em formato de totais");
            } else if (args[1].equalsIgnoreCase("1")) {
                manualSaveDataToTxt(getWeeklyData(dataAcumulated, daysFirstInterval, parametersToGetAcumulated),
                        args[19], "Dados semanais em formato de novos casos.");
                manualSaveDataToTxt(getWeeklyData(dataTotal, daysFirstInterval, parametersToGetTotal), args[19],
                        "Dados semanais em formato total.");
            } else {
                manualSaveDataToTxt(getMonthlyData(dataAcumulated, daysFirstInterval, parametersToGetAcumulated),
                        args[19], "Dados mensais em formato de novos casos.");
                manualSaveDataToTxt(getMonthlyData(dataTotal, daysFirstInterval, parametersToGetTotal), args[19],
                        "Dados mensais em formato total.");
            }

            // este ciclo serve para fazer o calculo das comparações para todas as colunas
            for (int i = 1; i < 6; i++) {
                if (i + 1 < 6) {
                    parametersForComparisonAcumulated[5] = Integer.toString(i + 1);
                    parametersForComparisonTotal[5] = Integer.toString(i + 1); // os parametros não atualizam dentro do
                    // for e por isso tem de estar sempre +1 a frente

                    manualSaveDataToTxt(comparisonMode(parametersForComparisonAcumulated), args[19],
                            "Comparação de novos casos diários.");
                    avgAndStdDeviation = calculateStdDeviation(comparisonMode(parametersForComparisonAcumulated),
                            calculateAverage(comparisonMode(parametersForComparisonAcumulated)));
                    manualSaveDataToTxt(avgAndStdDeviation, args[19], "");

                    manualSaveDataToTxt(comparisonMode(parametersForComparisonTotal), args[19],
                            "Comparação de total de casos.");
                    avgAndStdDeviation = calculateStdDeviation(comparisonMode(parametersForComparisonTotal),
                            calculateAverage(comparisonMode(parametersForComparisonTotal)));
                    manualSaveDataToTxt(avgAndStdDeviation, args[19], "");
                }
            }
            callMarkovMethods(args, 3);

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * verifica todos os campos e envia alerta de erros caso alguma informaçaão não
     * esteja como foi pedida
     *
     * @param args argumentos introduzidos pelo utilizador
     */
    public static void errorVerification(String[] args) {
        try {
            if (!args[0].equalsIgnoreCase("-r")) {
                System.out.println("-");
                System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                System.out.println("Por favor introduza o parâmetro -r antes de iniciar.");
                System.out.println("-");
                System.exit(0);
            }
            if (!args[1].equalsIgnoreCase("0") && !args[1].equalsIgnoreCase("1") && !args[1].equalsIgnoreCase("2")) {
                System.out.println("-");
                System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                System.out.println("Por favor introduza o intervalo temporal (0) diário, (1) semanal, (2) mensal. ");
                System.out.println("-");
                System.exit(0);
            }
            if (!args[2].equalsIgnoreCase("-di")) {
                System.out.println("-");
                System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                System.out.println("Por favor introduza o parâmetro -di antes da data de inicio. ");
                System.out.println("-");
                System.exit(0);
            }
            if (!args[4].equalsIgnoreCase("-df")) {
                System.out.println("-");
                System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                System.out.println("Por favor introduza o parâmetro -df antes da data de fim.");
                System.out.println("-");
                System.exit(0);
            }
            if (!args[6].equalsIgnoreCase("-di1")) {
                System.out.println("-");
                System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                System.out.println("Por favor introduza o parâmetro -di1 antes da data de inicio.");
                System.out.println("-");
                System.exit(0);
            }
            if (!args[8].equalsIgnoreCase("-df1")) {
                System.out.println("-");
                System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                System.out.println("Por favor introduza o parâmetro -df1 antes da data de fim.");
                System.out.println("-");
                System.exit(0);
            }
            if (!args[10].equalsIgnoreCase("-di2")) {
                System.out.println("-");
                System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                System.out.println("Por favor introduza o parâmetro -di2 antes da data de inicio.");
                System.out.println("-");
                System.exit(0);
            }
            if (!args[12].equalsIgnoreCase("-df2")) {
                System.out.println("-");
                System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                System.out.println("Por favor introduza o parâmetro -df2 antes da data de fim.");
                System.out.println("-");
                System.exit(0);
            }
            if (args.length == 20) {
                if (!args[14].equalsIgnoreCase("-T")) {
                    System.out.println("-");
                    System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                    System.out.println("Por favor introduza o parâmetro -T antes da data a realizar previsões.");
                    System.out.println("-");
                    System.exit(0);
                }

                if (!args[16].contains(".csv")) {
                    System.out.println("-");
                    System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                    System.out.println("Por favor introduza o ficheiro de numeros totais.");
                    System.out.println("-");
                    System.exit(0);
                }
                if (!args[17].contains(".csv")) {
                    System.out.println("-");
                    System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                    System.out.println("Por favor introduza o ficheiro de numeros acumulados.");
                    System.out.println("-");
                    System.exit(0);
                }
                if (!args[18].contains(".txt")) {
                    System.out.println("-");
                    System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                    System.out.println("Por favor introduza o ficheiro da matriz de transição.");
                    System.out.println("-");
                    System.exit(0);
                }
                if (!args[19].contains(".txt")) {
                    System.out.println("-");
                    System.out.println("Por favor introduza o nome do ficheiro de saida.");
                    System.out.println("-");
                    System.exit(0);
                }
            } else {
                if (!args[14].contains(".csv")) {
                    System.out.println("-");
                    System.out.println("Foi encontrado pelo menos 1 erro de execução.");
                    System.out.println("Por favor introduza o ficheiro de numeros acumulados.");
                    System.out.println("-");
                    System.exit(0);
                }
                if (!args[15].contains(".txt")) {
                    System.out.println("-");
                    System.out.println("Por favor introduza o nome do ficheiro de saida.");
                    System.out.println("-");
                    System.exit(0);

                }
            }

            File fileDataTotal = new File(" ");
            File fileDataAcumulated = new File(" ");

            if (args.length == 20) {
                fileDataTotal = new File(args[16]);
                fileDataAcumulated = new File(args[17]);
            }
            if (args.length == 16) {
                // fileDataAcumulated = new File("LAPR1_GRUPO03_COVID19/files/" + args[14]);
                fileDataAcumulated = new File(args[14]);
            }

            checkDatesValidity(args[3], args[5]);
            checkDatesValidity(args[7], args[9]);
            checkDatesValidity(args[11], args[13]);

            String[] datesForValidation = {args[3], args[5], args[7], args[9], args[11], args[13]};
            boolean dataValidation = true;

            for (int i = 0; i < 6; i++) {
                if (args.length == 20) {
                    dataValidation = validateDateInFile(invertDateFormat(datesForValidation[i]).toString(),
                            fileDataAcumulated);
                    if (!dataValidation) {
                        System.out.println("-");
                        System.out.println("A data " + datesForValidation[i] + " não existe no ficheiro "
                                + fileDataAcumulated + ".");
                        System.out.println("-");
                        System.exit(0);
                    }
                    dataValidation = validateDateInFile(datesForValidation[i], fileDataTotal);
                    if (!dataValidation) {
                        System.out.println("-");
                        System.out.println(
                                "A data " + datesForValidation[i] + " não existe no ficheiro " + fileDataTotal + ".");
                        System.out.println("-");
                        System.exit(0);
                    }
                } else {
                    dataValidation = validateDateInFile(invertDateFormat(datesForValidation[i]).toString(),
                            fileDataAcumulated);
                    if (!dataValidation) {
                        System.out.println("-");
                        System.out.println("A data " + datesForValidation[i] + " não existe no ficheiro "
                                + fileDataAcumulated + ".");
                        System.out.println("-");
                        System.exit(0);
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * procura a data anterior mais próxima à data indicada pelo utilizador como parâmetro -T
     *
     * @param startDate data escolhida pelo utilizador
     * @param file      ficheiro de dados
     * @param exists    valor que indica se a data indicada pelo existe no ficheiro
     * @return data anterior mais próxima à data indicada pelo utilizador
     */
    public static LocalDate findClosestDate(String startDate, File file, boolean exists) {
        LocalDate closestDate = null;

        try {
            String line = "";
            boolean flag = false;
            String[] finalLine = null;
            String previousLine = "";

            reader = new Scanner(file);
            reader.nextLine();

            if (exists) {
                while (reader.hasNextLine() && !flag) {
                    previousLine = line;
                    line = reader.nextLine();
                    if (line.contains(startDate)) {
                        flag = true;
                    }

                }
                finalLine = previousLine.split(",");
            } else {
                while (reader.hasNextLine() && !flag) {
                    line = reader.nextLine();
                    if (line.contains(startDate)) {
                        flag = true;
                    }
                }
                finalLine = line.split(",");
            }

            String date = finalLine[0];
            closestDate = convertFromDateToLocalDate(date);

        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            reader.close();
        }

        return closestDate;
    }


    /**
     * @param args argumentos inseridos pelo utilizador
     * @param mode modo 1: interactivo
     *             modo 2: manual só com previsões
     *             modo 3: manual completo
     */
    public static void callMarkovMethods(String[] args, int mode) {
        try {

            File fileCovidData = null;
            File markovFile = null;
            String txtTitle = "";
            LocalDate closestDate = null;

            String date = "";

            switch (mode) {
                case 1:
                    date = args[1];
                    fileCovidData = new File(args[3]);
                    markovFile = new File(args[6]);
                    break;

                case 2:
                    date = args[1];
                    fileCovidData = new File(args[2]);
                    markovFile = new File(args[3]);
                    txtTitle = args[4];
                    break;

                case 3:
                    date = args[15];
                    fileCovidData = new File(args[16]);
                    markovFile = new File(args[18]);
                    txtTitle = args[19];
                    break;

                default:
                    System.out.println("Não é possível realizar a previsão.");
            }

            boolean validateDate = validateDateInFile(date, fileCovidData);
            closestDate = findClosestDate(date, fileCovidData, validateDate);
            String[][] dataFromClosestDate = getArrayFromClosestDate(closestDate, fileCovidData);

            String[][] markovData = readMarkovFile(markovFile, 1);
            copyMarkovMatrix(markovData);

            if (mode == 2 || mode == 3) {
                manualSaveDataToTxt(markovData, txtTitle, "simplesmente printa o ficheiro de markov");

                String[][] markovResults = calculateMarkovMatrixAplication(markovData, dataFromClosestDate);
                manualSaveDataToTxt(markovResults, txtTitle, "previsões de Markov");
            }


        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    /**
     * trata os dados para poder chamar os métodos de Markov e aplicar as previsões
     *
     * @param args argumentos inseridos pelo utilizador
     */
    public static void callManualModePredictions(String[] args) {

        try {
            String date = args[1];
            LocalDate predictionDate = convertFromDateToLocalDate(date);

            int days = 2;
            int typeOfFile = 2;

            File fileCovidData = new File(args[2]);

            String txtTitle = args[4];

            boolean dataToValidate = validateDateInFile(date, fileCovidData);
            LocalDate closestDate = findClosestDate(date, fileCovidData, dataToValidate);
            String[][] data = null;
            if (!dataToValidate) {
                System.out.println("-");
                System.out.println("A data especificada não existe no ficheiro");
                System.out.println("-");
                data = readData(fileCovidData, days, closestDate, typeOfFile);

            } else {
                data = readData(fileCovidData, days, predictionDate, typeOfFile);

            }
            String dateToSave = "Day chosen by user to be read from total covid19 file";
            boolean teste = checkIfFileExists(args[3]);
            System.out.println(teste);

            String[] dataValidated = {"0", "0", "0", "0", "2", "1"};

            String[][] dataToShowUser = getDailyData(data, days, dataValidated);
            manualSaveDataToTxt(dataToShowUser, txtTitle, dateToSave);

            callMarkovMethods(args, 2);
            /*String[][] identityMatrix = createIdentityMatrix();
            manualSaveDataToTxt(identityMatrix, txtTitle, "matrix identidade");

            String[][] calculateLowerMatrix = matrixLower(markovFile);
            manualSaveDataToTxt(calculateLowerMatrix, txtTitle, "está a printar a matriz inferior");

            String[][] testeMatrix = copyMarkovMatrix(markovData);
            manualSaveDataToTxt(testeMatrix, txtTitle, "teste para matrix mais pequena");

            String[][] calculateUpperMatrix = matrixUpper(markovFile);
            manualSaveDataToTxt(calculateUpperMatrix, txtTitle, "está a printar a matriz superior");*/

            // String[][] showLUMatrix = multiplicationMatrix(calculateLowerMatrix,
            // calculateUpperMatrix);
            // manualSaveDataToTxt(showLUMatrix, txtTitle, "teste de Markov");

        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    /**
     * @param closestDate data anterior mais próxima à data inserida pelo utilizador
     * @param file        ficheiro com dados covid
     * @return array bidimensional com os dados da data indicada
     */
    public static String[][] getArrayFromClosestDate(LocalDate closestDate, File file) {
        String[][] dataFromClosestDate = new String[1][COLUMNS - 1];
        try {
            reader = new Scanner(file);
            reader.nextLine();
            String line = "";
            String[] values = null;
            String date = null;

            while (reader.hasNextLine()) {
                line = reader.nextLine();
                values = line.split(",");
                date = String.valueOf(invertDateFormat(values[0]));
                if (date.equals(String.valueOf(closestDate))) {
                    for (int i = 1; i < values.length; i++) {
                        dataFromClosestDate[0][i - 1] = values[i];
                    }
                }
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            reader.close();
        }
        return dataFromClosestDate;
    }

    /**
     * este metodo vai questionar o utilizador que funcionalidade quer executar, ou
     * se quer sair da aplicação
     *
     * @param receivedParameters parâmetros recebidos do utilizador
     */
    public static void callInteractiveMode(String[] receivedParameters) {

        try {
            int input;
            clearConsole();

            do {
                reader = new Scanner(System.in);

                System.out.println("---------------------------");
                System.out.println("M E N U   P R I N C I P A L");
                System.out.println("---------------------------");
                System.out.println("(1) Introdução de dados");
                System.out.println("(2) Análise Comparativa");
                System.out.println("(3) Previsões");
                System.out.println("(4) Carregar ficheiro");
                System.out.println("(5) Sair");
                System.out.println("---------------------------");
                System.out.print("Indique a sua opção: ");

                input = reader.nextInt();

                switch (input) {
                    case 1:
                        showGetDataMenu(receivedParameters);
                        break;
                    case 2:
                        getInfoForComparisonMode(receivedParameters);
                        break;
                    case 3:
                        if (!checkIfFileWasLoaded(receivedParameters)) {
                            clearConsole();
                            callInteractiveMode(receivedParameters);
                        } else {
                            if (Objects.equals(receivedParameters[4], "1")) {
                                System.out.println("O ficheiro carregado é de acumulados.");
                                System.out.println("Por favor carregue um ficheiro de totais.");
                                pressEnterToContinue();
                            } else {
                                if (Objects.equals(receivedParameters[6], "-")) {
                                    System.out.println(
                                            "Para esta funcionalidade é necessário carregar um ficheiro de Markov.");
                                    System.out.println(
                                            "No Menu Principal, aceda à opção 'Carregar Ficheiro' e depois seleccione a respectiva opção.");
                                    pressEnterToContinue();
                                    clearConsole();
                                } else {
                                    getInfoForPredictionsMode(receivedParameters);
                                }
                            }
                        }
                        break;
                    case 4:
                        showFileMenu(receivedParameters);
                        break;
                    case 5:
                        System.out.println("-");
                        System.out.println("Goodbye");
                        System.out.println("-");
                        break;
                    default:
                        System.out.println("-");
                        System.out.println("Opção inválida!");
                        System.out.println("-");
                        break;
                }

            } while (input != 5);

            System.exit(1);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    /**
     * obtém a informação necessária para aplicar os métodos de Markov
     *
     * @param parametersValidated parâmetros inseridos pelo utilizador, validados
     */
    private static void getInfoForPredictionsMode(String[] parametersValidated) {

        try {
            reader = new Scanner(System.in);

            if (checkIfFileExists(parametersValidated[6])) {
                pressEnterToContinue();
                clearConsole();
                getStartAndEndDate(parametersValidated, 3);
            } else {
                System.out.println("-");
                System.out.println("O ficheiro não existe.");
                System.out.println("-");
                pressEnterToContinue();
                clearConsole();
                callInteractiveMode(parametersValidated);
            }

            checkInputValidityMarkov(parametersValidated);
            if (!checkIfFileExists(parametersValidated[6])) {
                System.out.println("-");
                System.out.println("O ficheiro não existe.");
                System.out.println("-");
                System.exit(0);
            }

            callMarkovMethods(parametersValidated, 1);

            pressEnterToContinue();

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Método que vai obter a informação ainda necessária para colocar no array e
     * chamar o método de comparação
     * O array (comparisonParameters) tem 7 posições:
     * [0] = data de inicio 1
     * [1] = data de fim 1
     * [2] = data de inicio 2
     * [3] = data de fim 2
     * [4] = tipo de ficheiro (acumulados/totais):
     * 0 = nada;
     * 1 = acumulados;
     * 2 = totais
     * [5] = dados a tratar:
     * 1 = todas;
     * 2 = infectados;
     * 3 = hospitalizados;
     * 4 = internados UCI;
     * 5 = mortos
     * [6] = nome do ficheiro
     *
     * @param parametersValidated parâmetros validados
     */
    public static void getInfoForComparisonMode(String[] parametersValidated) {
        try {
            String[] comparisonParameters = new String[7];

            if (checkIfFileWasLoaded(parametersValidated)) {
                showGetDataTypeMenu(parametersValidated, 1, 1);
                comparisonParameters[5] = parametersValidated[5];
                comparisonParameters[6] = parametersValidated[3];
                comparisonParameters[4] = parametersValidated[4];

                getStartAndEndDate(parametersValidated, 1);
                comparisonParameters[0] = parametersValidated[1];
                comparisonParameters[1] = parametersValidated[2];

                getStartAndEndDate(parametersValidated, 2);
                comparisonParameters[2] = parametersValidated[1];
                comparisonParameters[3] = parametersValidated[2];

                checkInputValidity(comparisonParameters);
                if (!checkIfFileExists(comparisonParameters[6])) {
                    System.out.println("-");
                    System.out.println("O ficheiro não existe.");
                    System.out.println("-");
                    System.exit(0);
                }
                checkDatesValidity(comparisonParameters[0], comparisonParameters[1]);
                checkDatesValidity(comparisonParameters[2], comparisonParameters[3]);
            }

            checkDatesValidity(comparisonParameters[0], comparisonParameters[1]);
            checkDatesValidity(comparisonParameters[2], comparisonParameters[3]);
            clearConsole();

            String dataToShow[][] = comparisonMode(comparisonParameters);
            String[][] avgAndStdDeviation = calculateStdDeviation(comparisonMode(comparisonParameters),
                    calculateAverage(comparisonMode(comparisonParameters)));
            String[][] dataToShowWithMediaAndStd = new String[dataToShow.length + 3][5];
            mergeTwoMatrixInOne(dataToShow, avgAndStdDeviation, dataToShowWithMediaAndStd);
            showOrSave(dataToShowWithMediaAndStd);

        } catch (Exception exception) {
            System.out.println();
            System.out.println("Por favor introduza informação válida");
            System.out.println();
            pressEnterToContinue();
            getInfoForComparisonMode(parametersValidated);
        }
    }

    /**
     * valida os parâmetros introduzidos pelo utilizador
     *
     * @param parameters parâmetros a validar
     */
    public static void checkDataValidity(String[] parameters) {
        try {
            checkInputValidity(parameters);
            checkTempResolution(parameters);
            if (!checkIfFileExists(parameters[3])) {
                System.out.println("-");
                System.out.println("O ficheiro não existe.");
                System.out.println("-");
                System.exit(0);
            }
            checkDatesValidity(parameters[1], parameters[2]);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public static void checkInputValidityMarkov(String[] userInput) {
        try {

            if (userInput[1] == null || userInput[3] == null || userInput[4] == null || userInput[6].equals("-")) {
                System.out.println("-");
                System.out.println("Utilizador colocou dados formato errado");
                System.out.println("-");
                System.exit(0);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * valida que o utilizador não enviou parâmetros a null
     *
     * @param userInput parâmtros inseridos pelo utilizador
     */
    public static void checkInputValidity(String[] userInput) {

        try {

            for (int i = 0; i < userInput.length; i++) {
                if (userInput[i] == null) {
                    System.out.println("-");
                    System.out.println("Utilizador colocou dados formato errado");
                    System.out.println("-");
                    System.exit(0);
                }
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * valida se o utilizador introduziu uma resolução temporal correcta
     *
     * @param userInput
     */
    public static void checkTempResolution(String[] userInput) {
        try {
            switch (userInput[0]) {
                case "0":
                case "1":
                case "2":
                    break;
                default:
                    System.exit(0);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    /**
     * método que converte a data recebida no formato dd-mm-yyyy para Date e depois
     * para LocalDate
     *
     * @param receivedDate data inserida pelo utilizador
     * @return localDate - data convertida
     */
    public static LocalDate convertFromDateToLocalDate(String receivedDate) {
        LocalDate localDate = null;

        try {
            Date receivedDateDate = new SimpleDateFormat("dd-MM-yyyy").parse(receivedDate);
            localDate = receivedDateDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } catch (Exception exception) {
            System.out.println();
            System.out.println("Por favor introduza as datas no formato correto.");
            System.out.println();
            pressEnterToContinue();
            String[] argsNull = {};
            startProgram(argsNull);
        }

        return localDate;
    }

    /**
     * inverte o formato da data de "dd-mm-yyyy" para "yyyy-mm-dd" para os métodos
     * seguintes
     *
     * @param receivedDate data cujo formato precisa ser modificado
     * @return localDate data no formato LocalDate
     * @throws ParseException
     */
    public static LocalDate invertDateFormat(String receivedDate) {
        LocalDate localDate = null;

        try {
            String[] date = receivedDate.split("-");
            String day = date[0];
            String month = date[1];
            String year = date[2];

            String newDate = year + "-" + month + "-" + day;

            localDate = LocalDate.parse(newDate);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return localDate;
    }

    /**
     * valida o formato das datas inseridas pelo utilizador e se a date2 é posterior
     * à date1
     *
     * @param date1 primeira data inserida pelo utilizador
     * @param date2 segunda data inserida pelo utilizador
     */
    public static void checkDatesValidity(String date1, String date2) {

        LocalDate startDate = convertFromDateToLocalDate(date1);
        LocalDate endDate = convertFromDateToLocalDate(date2);

        try {
            if (startDate.isBefore(endDate)) {

            } else if (startDate.isAfter(endDate)) {
                System.out.println("-");
                System.out.println("A primeira data colocada pelo utilizador devera ser superior à segunda data");
                System.out.println("-");
                System.exit(0);
            } else {
                System.out.println("-");
                System.out.println("Data iguais apenas poderá ser considerada resolução diária");
                System.out.println("-");

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * conta o número de dias lógicos do calendário entre duas datas fornecidas pelo
     * utilizador
     *
     * @param startDate data de início do intervalo pretendido
     * @param endDate   data de fim do intervalo pretendido
     * @return int days dias lógicos do calendário entra a data de início
     * e a data de fim do intervalo indicado pelo utilizador
     */
    public static int countDays(LocalDate startDate, LocalDate endDate) {
        int days = 0;

        try {
            days = (int) DAYS.between(startDate, endDate);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return days;
    }

    /**
     * lê os dados do ficheiro .csv fornecido para uma matriz de strings
     *
     * @param file ficheiro fornecido pelo utilizador
     * @param days dias lógicos entre as datas fornecidas pelo utilizador
     * @return String[][] data array bidimensional com os dados do ficheiro
     * fornecido para o intervalo de dias especificado
     */
    public static String[][] readData(File file, int days, LocalDate startDate, int typeOfFile) {

        String[][] data = new String[days][COLUMNS];

        try {
            reader = new Scanner(file);
            reader.nextLine();
            int counter = 0;
            boolean flag = false;

            switch (typeOfFile) {
                case 1:
                    while (reader.hasNextLine() && !flag) {
                        String line = reader.nextLine();
                        String[] values = line.split(",");

                        LocalDate dateToAssess = LocalDate.parse(values[0]);
                        LocalDate previousDate = startDate.minusDays(1);

                        if (isSameDate(dateToAssess, previousDate)) {
                            String lineInside = line;
                            String[] valuesInside = line.split(",");
                            for (int j = 0; j < valuesInside.length; j++) {
                                data[counter][j] = valuesInside[j];
                            }
                            counter++;
                            while (reader.hasNextLine() && counter < days) {
                                lineInside = reader.nextLine();
                                valuesInside = lineInside.split(",");
                                for (int j = 0; j < valuesInside.length; j++) {
                                    data[counter][j] = valuesInside[j];
                                }
                                counter++;
                            }
                            if (counter == days) {
                                flag = true;
                            }
                        }
                    }
                    break;
                case 2:
                    while (reader.hasNextLine() && !flag) {
                        String line = reader.nextLine();
                        String[] values = line.split(",");

                        values[0] = String.valueOf(invertDateFormat(values[0]));

                        LocalDate dateToAssess = LocalDate.parse(values[0]);
                        LocalDate previousDate = startDate.minusDays(1);

                        if (isSameDate(dateToAssess, previousDate)) {
                            String lineInside = line;
                            String[] valuesInside = line.split(",");
                            valuesInside[0] = String.valueOf(invertDateFormat(valuesInside[0]));
                            for (int j = 0; j < valuesInside.length; j++) {
                                data[counter][j] = valuesInside[j];
                            }
                            counter++;
                            while (reader.hasNextLine() && counter < days) {
                                lineInside = reader.nextLine();
                                valuesInside = lineInside.split(",");
                                valuesInside[0] = String.valueOf(invertDateFormat(valuesInside[0]));
                                for (int j = 0; j < valuesInside.length; j++) {
                                    data[counter][j] = valuesInside[j];
                                }
                                counter++;
                            }
                            if (counter == days) {
                                flag = true;
                            }
                        }
                    }
                    break;

                default:
                    System.out.println("-");
                    System.out.println("Opção inválida");
                    System.out.println("-");
                    break;
            }
        } catch (Exception exception) {

        } finally {
            reader.close();
        }
        if (data[0][0] == null) {
            System.out.println();
            System.out.println("Não podemos calcular os dados porque a data de inicio coincide com a data inicial do ficheiro.");
            System.out.println("Por favor escolha outra data de inicio válida.");
            pressEnterToContinue();
            String[] empyVector = {};
            startProgram(empyVector);
        }
        return data;


    }

    /**
     * valida que a data indicada pelo utilizador existe no ficheiro de dados que
     * indicou
     *
     * @param date data a validar
     * @param file ficheiro escolhido pelo utilizador
     * @return true se a data existir no ficheiro, false no caso contrário
     */
    public static boolean validateDateInFile(String date, File file) {
        boolean flag = false;

        try {
            reader = new Scanner(file);
            reader.nextLine();
            while (reader.hasNextLine() && !flag) {
                String line = reader.nextLine();
                if (line.contains(date)) {
                    flag = true;
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            reader.close();
        }

        return flag;

    }

    /**
     * Método que verifica se a data a avaliar é igual à data de início do intervalo
     * especificado pelo utilizador
     *
     * @param dateToAssess data a avaliar, correspondente a uma linha do ficheiro a
     *                     ser lido
     * @param startDate    data de início do intervalo de tempo a ser considerado
     * @return flag
     */
    public static boolean isSameDate(LocalDate dateToAssess, LocalDate startDate) {
        boolean flag = false;

        try {
            if (String.valueOf(dateToAssess).equals(String.valueOf(startDate))) {
                flag = true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return flag;
    }

    /**
     * metodo apenas para verificar qual o time frame escolhido pelo utilizador
     *
     * @param data               matriz original
     * @param parametersValidity parametros introduzidos pelo utilizador
     * @param days               numero de dias do intervalo de datas
     */
    public static String[][] chooseTimeFrame(String[][] data, String[] parametersValidity, int days) {
        String[][] dataToShow = new String[0][0];

        try {
            switch (parametersValidity[0]) {
                case "0":
                    dataToShow = getDailyData(data, days, parametersValidity);
                    break;
                case "1":
                    dataToShow = getWeeklyData(data, days, parametersValidity);
                    break;
                case "2":
                    dataToShow = getMonthlyData(data, days, parametersValidity);
                    break;
                default:
                    System.out.println("-");
                    System.out.println("Time frame invalido");
                    System.out.println("-");
                    System.exit(0);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return dataToShow;
    }

    /**
     * recolhe os dados obtidos do ficheiro e apresenta em forma de tabela para o
     * utilizador
     *
     * @param data                dados originais com 1 dia antes da data original
     * @param days                numero total de dias da matriz
     * @param validatedParameters parâmetros inseridos pelo utilizador, validados
     * @return dados correspondentes ao período que o utilizador escolheu
     */
    public static String[][] getDailyData(String[][] data, int days, String[] validatedParameters) {

        String[][] dataToShow = null;

        try {
            int day = 1, j = 1;

            // este metodo vai analisar quantas colunas vai mostrar, se são todas ou apenas uma
            int numberOfColunmsToShow = numberOfColumnsToShow(validatedParameters[5]);

            // dependendo da escolha do utilizador ele escolhe a coluna a ser exibida
            int firstColunm = chooseFirstColumn(validatedParameters[5]);

            // dimensiona a matriz para numero de colunas e linhas necessario
            dataToShow = new String[days][numberOfColunmsToShow];

            // preenche o cabeçalho da matriz
            fillFirstLine(dataToShow, numberOfColunmsToShow, firstColunm);

            // este ciclo preenche a matriz com os valores e os calculos realizados para o
            // caso de ser ficheiro de acumulados ou totais
            for (int i = 0; i < days; i++) {
                if (i + 1 < days)
                    dataToShow[day][0] = data[i + 1][0];
                for (j = 1; j < numberOfColunmsToShow; j++) {
                    if (i + 1 < days)
                        if (validatedParameters[4].equalsIgnoreCase("1")) {
                            // se for "1" vai mostrar todas as colunas, caso não seja "1" vai mostrar apenas a coluna seleccionada
                            if (firstColunm == 1) {
                                dataToShow[day][j] = Integer.toString(
                                        Integer.parseInt(data[i + 1][j + 1]) - Integer.parseInt(data[i][j + 1]));
                            } else {
                                dataToShow[day][j] = Integer.toString(Integer.parseInt(data[i + 1][firstColunm])
                                        - Integer.parseInt(data[i][firstColunm]));
                            }
                        } else {
                            if (firstColunm == 1) {
                                dataToShow[day][j] = data[i + 1][j + 1];
                                // como o array data traz 1 dia a mais o +1 serve para ignorar isso
                                // e comecar no que interessa
                            } else {
                                dataToShow[day][j] = data[i + 1][firstColunm];
                            }
                        }
                }
                day++;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return dataToShow;
    }

    /**
     * serve apenas para criar o cabeçalho da matriz para exibiçaão
     *
     * @param dataToShow            matriz original
     * @param numberOfColumnsToShow numero de colunas a exibir
     * @param firstColumn           coluna a exibir
     */
    public static void fillFirstLine(String[][] dataToShow, int numberOfColumnsToShow, int firstColumn) {
        try {
            String column = checkNameOfFirstColumn(firstColumn);
            if (numberOfColumnsToShow == 5) {
                dataToShow[0][0] = "Data";
                dataToShow[0][1] = "Infectado";
                dataToShow[0][2] = "Hospitalizado";
                dataToShow[0][3] = "Internados UCI";
                dataToShow[0][4] = "Mortes";
            } else {
                dataToShow[0][0] = "Data";
                dataToShow[0][1] = column;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * quando é necessáro apenas mostrar 1 coluna é neste metodo que isso é
     * definido,
     * variando consoante a escolha do utilizador
     *
     * @param firstColumn coluna escolhida
     * @return nome da coluna escolhida
     */
    public static String checkNameOfFirstColumn(int firstColumn) {
        String result = "";
        try {
            switch (firstColumn) {
                case 2:
                    result = "Infectados";
                    break;
                case 3:
                    result = "Hospitalizados";
                    break;
                case 4:
                    result = "Internado UCI";
                    break;
                case 5:
                    result = "Mortes";
                    break;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

    /**
     * metodo que recebe matriz e escreve direto no txt
     *
     * @param data              matriz original
     * @param txtName           nome do ficheiro a gravar
     * @param titleOfDataToSave nome para divisoria entre informação
     * @throws IOException
     */

    public static void manualSaveDataToTxt(String[][] data, String txtName, String titleOfDataToSave) throws IOException {

        File DataFromManualToTxt = new File(txtName);

        FileWriter writeUserTxtData = new FileWriter(DataFromManualToTxt, true);

        try {
            if (DataFromManualToTxt.exists()) {
                if (!titleOfDataToSave.equals("")) {
                    writeUserTxtData.append("***************** " + titleOfDataToSave + " *****************");
                    writeUserTxtData.append("\n");
                    writeUserTxtData.append("\n");
                }
                for (int i = 0; i < data.length; i++) {
                    for (int j = 0; j < data[i].length; j++) {
                        // reserva 15 espacos para cada valor e encosta a esquerda
                        writeUserTxtData.append(String.format("%-15s", data[i][j]));
                        writeUserTxtData.append(" ");
                    }
                    writeUserTxtData.append("\n");
                }
            } else {
                DataFromManualToTxt.createNewFile();
                System.out.println("-");
                System.out.println("foi criado um ficheiro txt, com o nome " + DataFromManualToTxt.getName());
                System.out.println("-");
                writeUserTxtData.append("***************** " + titleOfDataToSave + " *****************");
                writeUserTxtData.append("\n");
                for (int i = 0; i < data.length; i++) {
                    for (int j = 0; j < data[i].length; j++) {
                        writeUserTxtData.append(String.format("%-15s", data[i][j]));
                        writeUserTxtData.append(" ");
                    }
                    writeUserTxtData.append("\n");
                }
            }
            writeUserTxtData.append("\n");

        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            writeUserTxtData.close();
        }
    }

    /**
     * soma todos os dados diarios durante uma semana e apresenta ao utilizador
     * * a semana esta configurada para segunda a domingo
     * * semanas nao completas serão ignoradas
     *
     * @param data                matriz original
     * @param days                dias totais da matriz
     * @param validatedParameters parâmetros inseridos pelo utilizador, validados
     * @return dados correspondentes ao período escolhido pelo utilizador
     */
    public static String[][] getWeeklyData(String[][] data, int days, String[] validatedParameters) {
        String[][] dataToShow = null;
        try {
            LocalDate date = LocalDate.parse(data[1][0]);
            // transforma a data que vem em string para formato de data

            String dayOfWeek = String.valueOf(date.getDayOfWeek());
            // verifica que dia da semana é a primeira data escolhida

            // calcula o numero de dias para chegar a segunda feira e comecar a contar a semana
            int numberDaysToMonday = checkDayOfTheWeek(dayOfWeek), i = 1, week = 1;

            // conta o numero de semanas a ser exibidas (segunda a domingo)
            int numberOfWeeks = howManyWeeks(numberDaysToMonday, days);

            // dependendo da escolha do utilizador define se eé para mostrar tudo ou apenas 1 coluna especifica
            int numberOfColumnsToShow = numberOfColumnsToShow(validatedParameters[5]);

            // no caso de ser escolhido apenas 1 coluna aqui diz qual é essa coluna (infectados, hospitalizados, internados, mortes)
            int firstColumn = chooseFirstColumn(validatedParameters[5]);

            // dimensiona o array para exibição
            dataToShow = new String[numberOfWeeks + 1][numberOfColumnsToShow];

            boolean loop = true;

            // ciclo para preencher a matriz com todos os dados, funciona para ficheiro de
            // acumulados ou de totais , dependendo do escolhido pelo utilizador
            if ((i + numberDaysToMonday) + 6 < days) {
                // preenche o cabecalho.
                fillFirstLine(dataToShow, numberOfColumnsToShow, firstColumn);

                // por definicao o metodo de preencher o cabecalho escreve "data" na primeira
                // coluna pois é chamado por varios metodos, neste caso especifico temos de mudar para semana
                dataToShow[0][0] = "Semana ";
                for (i = i + numberDaysToMonday; i + 6 < days; i = i + 7) {
                    dataToShow[week][0] = "Semana " + week;
                    for (int j = 1; j < numberOfColumnsToShow; j++) {
                        if (validatedParameters[4].equalsIgnoreCase("1")) {
                            // se for "1" vai exibir todas as colunas, se nao for "1" vai exibir apenas a coluna escolhida pelo utilizador
                            if (firstColumn == 1) {
                                dataToShow[week][j] = Integer.toString(
                                        Integer.parseInt(data[i + 6][j + 1]) - Integer.parseInt(data[i - 1][j + 1]));
                            } else {
                                dataToShow[week][j] = Integer.toString(Integer.parseInt(data[i + 6][firstColumn])
                                        - Integer.parseInt(data[i - 1][firstColumn]));
                            }
                        } else {
                            // o programa entra aqui no caso de estar a tratar de ficheiros com totais que é so somar tudo
                            if (firstColumn == 1) {
                                dataToShow[week][j] = sumTotalWeek(data, i, j + 1);
                            } else {
                                dataToShow[week][j] = sumTotalWeek(data, i, firstColumn);
                            }
                        }
                    }
                    week++;
                }
            } else {
                dataToShow = new String[][]{{"-----------------------------"}, {"Não existem semanas completas"},
                        {"-----------------------------"}};

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return dataToShow;
    }

    /**
     * com este metodo o programa vai saber qual coluna especifica é para mostrar,
     * ou se eé para mostrar todas
     *
     * @param numberOfColumns numero da coluna escolhida pelo utilizador
     * @return número da coluna escolhida pelo utilizador
     */
    public static int chooseFirstColumn(String numberOfColumns) {

        int result = 0;

        try {
            result = Integer.parseInt(numberOfColumns);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

    /**
     * com este metodo o programa sabe quantas colunas sao para exibir na consola
     * para assim dimensionar o array
     *
     * @param numberOfColumns numero de colunas esolhidas pelo utilizador
     * @return número de colunas a mostrar
     */
    public static int numberOfColumnsToShow(String numberOfColumns) {
        int result = 0;
        try {
            switch (numberOfColumns) {
                case "1":
                    result = 5;
                    break;
                default:
                    result = 2;
                    break;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

    /**
     * quantas semanas tem o intervalo de datas escolhido
     *
     * @param numberDaysToMonday numero de dias da primeira data ate chegar a
     *                           segunda feira
     * @param days               dias totais da matriz
     * @return número de semanas completas
     */
    public static int howManyWeeks(int numberDaysToMonday, int days) {
        int numberOfWeeks = 0;

        try {
            for (int i = 1 + numberDaysToMonday; i < days; i = i + 7) {
                if (i + 6 < days)
                    numberOfWeeks++;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return numberOfWeeks;
    }

    /**
     * metodo apenas para verificar que dia da semana o utilizador escolheu para
     * começar
     * com isto conseguimos garantir que a semana comeca na segunda-feira
     *
     * @param dayOfWeek dia escolhido pelo utilizador
     * @return nome do dia da semana
     */
    public static int checkDayOfTheWeek(String dayOfWeek) {
        int result = 0;

        try {
            switch (dayOfWeek) {
                case "MONDAY":
                    break;
                case "TUESDAY":
                    result = 6;
                    break;
                case "WEDNESDAY":
                    result = 5;
                    break;
                case "THURSDAY":
                    result = 4;
                    break;
                case "FRIDAY":
                    result = 3;
                    break;
                case "SATURDAY":
                    result = 2;
                    break;
                case "SUNDAY":
                    result = 1;
                    break;
                default:
                    System.out.println("-");
                    System.out.println("Não é dia da semana");
                    System.out.println("-");
                    result = -1;
                    break;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    /**
     * metodo para mostrar resolucao mensal escolhida pelo utilizador
     * apenas mostra meses completos
     * se não for possivel encontrar mes completo voltao ao menu para novas datas
     *
     * @param data                matriz original
     * @param days                dias completos da matriz
     * @param validatedParameters parâmetros inseridos pelo utilizador, validados
     * @return dados correspondentes ao período escolhido pelo utilizador
     */
    public static String[][] getMonthlyData(String[][] data, int days, String[] validatedParameters) {
        String[][] dataToShow = null;

        try {
            // converte a data que vem em string para formato localDate
            LocalDate date = LocalDate.parse(data[1][0]);

            // calcula o numero de colunas a exibir, pode ser apenas 1 ou todas
            int numberOfColumnsToShow = numberOfColumnsToShow(validatedParameters[5]);

            // no caso de ser escolhida apenas 1 dentro deste metodo para dizer qual das
            // colunas vai exibir
            int firstColumn = chooseFirstColumn(validatedParameters[5]);

            boolean outLoop = false;

            int firstDate = 1, valueTillFirstDay = 1, months = 0;

            // calucula o numero de meses a exibir para dimensionar a matriz
            int numberOfMonths = numberOfFullMonths(data, days);

            // dimensiona a matriz
            dataToShow = new String[numberOfMonths + 1][numberOfColumnsToShow];

            // preenche o cabeçalho da matriz
            fillFirstLine(dataToShow, numberOfColumnsToShow, firstColumn);

            // como o cabeçalho vem com "Data", aqui alteramos para "Mês"
            dataToShow[0][0] = "Mês";

            // so inicia se ouver meses completos a ser exibidos
            if (numberOfMonths > 0) {
                // se o primeiro dia escolhido nao for o primeiro do mes temos de saber quantos
                // dias faltam para acabar o mes
                // entao comecar no dia 1 do mes a seguir
                if (date.getDayOfMonth() != 1) {
                    firstDate = firstDate + howManyDaysToEndOfMonth(data[firstDate][0]) + valueTillFirstDay;
                }
                // verificamos se é possivel calcular o mes
                if (firstDate < days) {
                    // preenchemos a matriz enquanto for possivel adicionar os dias completos do
                    // propios mes e do mes a seguir
                    while (firstDate + howManyDaysToEndOfMonth(data[firstDate][0]) < days && !outLoop) {
                        date = LocalDate.parse(data[firstDate + 1][0]);
                        months++;
                        dataToShow[months][0] = "Mês " + months;
                        for (int i = 1; i < numberOfColumnsToShow; i++) {
                            // se o valor for "1" vai exibir todas as colunas, s enao for vai apenas exibir
                            // a coluna escolhida
                            if (validatedParameters[4].equalsIgnoreCase("1")) {
                                if (firstColumn == 1) {

                                    dataToShow[months][i] = Integer.toString(Integer.parseInt(data[firstDate - 1 + date.lengthOfMonth()][i + 1]) - Integer.parseInt(data[firstDate - 1][i + 1]));

                                } else {
                                    dataToShow[months][i] = Integer.toString(
                                            Integer.parseInt(data[firstDate - 1 + date.lengthOfMonth()][firstColumn])
                                                    - Integer.parseInt(data[firstDate - 1][firstColumn]));
                                }
                            } else {

                                if (firstColumn == 1) {
                                    dataToShow[months][i] = sumTotalMonth(data, i + 1, firstDate, date.lengthOfMonth());
                                } else {
                                    dataToShow[months][i] = sumTotalMonth(data, firstColumn, firstDate,
                                            date.lengthOfMonth());
                                }
                            }
                        }
                        // se existir dias para mais 1 mes o outloop fica false, se nao ouver dias
                        // suficientes sai do loop
                        if (firstDate + howManyDaysToEndOfMonth(data[firstDate][0]) + 1 < days) {
                            firstDate = firstDate + howManyDaysToEndOfMonth(data[firstDate][0]) + valueTillFirstDay;
                        } else {
                            outLoop = true;
                        }
                    }
                }
            } else {
                dataToShow = new String[][]{{"--------------------------------------"},
                        {"Não é possível saber resolução mensal."}, {"Não existem meses completos.          "},
                        {"--------------------------------------"}};
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return dataToShow;
    }

    /**
     * método para determinar o número de meses no intervalo de datas, que vai
     * permitir dimensionar o array
     *
     * @param data matriz contendo os dados do intervalo de datas
     * @param days número de dias no intervalo de datas
     * @return número de meses completos existentes no intervalo de datas
     */
    public static int numberOfFullMonths(String[][] data, int days) {
        int numberOfMonths = 0;
        try {
            LocalDate date = LocalDate.parse(data[1][0]);
            boolean outLoop = false;
            int firstDate = 1, timeUntilFirstDay = 1;
            if (date.getDayOfMonth() != 1) {
                firstDate = firstDate + howManyDaysToEndOfMonth(data[firstDate][0]) + timeUntilFirstDay;
            }
            if (firstDate < days) {
                while (firstDate + howManyDaysToEndOfMonth(data[firstDate][0]) < days && !outLoop) {
                    numberOfMonths++;
                    if (firstDate + howManyDaysToEndOfMonth(data[firstDate][0]) + 1 < days) {
                        firstDate = firstDate + howManyDaysToEndOfMonth(data[firstDate][0]) + timeUntilFirstDay;
                    } else {
                        outLoop = true;
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return numberOfMonths;
    }

    /**
     * identifica quantos dias faltam para o fim do mes para conseguir realizar
     * calculos
     *
     * @param receivedDate data do mes
     * @return dias ate ao fim do mes especifico
     */
    public static int howManyDaysToEndOfMonth(String receivedDate) {
        LocalDate date = LocalDate.parse(receivedDate);
        String dayOfMonth = String.valueOf(date.getDayOfMonth());
        String endOfMonth = String.valueOf(date.lengthOfMonth());
        return Integer.parseInt(endOfMonth) - Integer.parseInt(dayOfMonth);
    }

    /**
     * Este método é responsável por fazer o clear screen do ecran
     */
    public static void clearConsole() {
        try {
            String operatingSystem = System.getProperty("os.name"); //Check the current operating system
            if (operatingSystem.contains("Windows")) {
                ProcessBuilder pb = new ProcessBuilder("cmd", "/c", "cls");
                Process startProcess = pb.inheritIO().start();
                startProcess.waitFor();
            } else {
                ProcessBuilder pb = new ProcessBuilder("clear");
                Process startProcess = pb.inheritIO().start();
                startProcess.waitFor();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Este método emite mensagem e aguarda pela tecla <Enter>
     */
    public static void pressEnterToContinue() {
        System.out.println("");
        System.out.println("Pressione a tecla <Enter> para continuar...");
        try {
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Este método apresenta o menu que solicita ao utilizado informação sobre qual
     * o
     * tipo de resolução que pretende obter.
     *
     * @param validatedParameters parâmetros inseridos pelo utilizador, validados
     */
    public static void showGetDataMenu(String[] validatedParameters) {
        try {
            reader = new Scanner(System.in);
            int option = 0;

            clearConsole();

            if (checkIfFileWasLoaded(validatedParameters)) {

                do {
                    System.out.println("-------------------------------");
                    System.out.println("       TIPO DE RESOLUÇÃO       ");
                    System.out.println("-------------------------------");
                    System.out.println("(0) Diária");
                    System.out.println("(1) Semanal");
                    System.out.println("(2) Mensal");
                    System.out.println("(3) Regressar ao Menu Principal");
                    System.out.println("-------------------------------");
                    System.out.print("Indique a sua opção: ");

                    option = reader.nextInt();

                    switch (option) {
                        case 0:
                        case 1:
                        case 2:
                            clearConsole();
                            validatedParameters[0] = Integer.toString(option);
                            showGetDataTypeMenu(validatedParameters, 0, 0);
                            break;
                        case 3:
                            System.out.println("-");
                            System.out.println("Goodbye");
                            System.out.println("-");
                            clearConsole();
                            callInteractiveMode(validatedParameters);
                            break;
                        default:
                            System.out.println("-");
                            System.out.println("Opção inválida!");
                            System.out.println("-");
                            pressEnterToContinue();
                            clearConsole();
                            break;
                    }

                } while (option != 3);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Método que valida se um determinado ficheiro já foi ou não carregado,
     * retornando verdadeiro ou falso
     *
     * @param validatedParameters parâmetros inseridos pelo utilizador, validados
     * @return true se o ficheiro já tiver sido carregado, falso se ainda não tiver
     * sido carregado
     */
    public static boolean checkIfFileWasLoaded(String[] validatedParameters) {

        boolean fileLoaded = false;

        try {
            if (validatedParameters[3] == null) {
                System.out.println("-");
                System.out.println("Precisa de carregar o respectivo ficheiro primeiro.");
                System.out.println("No Menu Principal selecione a respectiva opção.");
                System.out.println("-");
                pressEnterToContinue();
                clearConsole();
            } else {
                fileLoaded = true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return fileLoaded;
    }

    /**
     * método que valida os dados inseridos pelo utilizador
     *
     * @param validatedParameters parâmetros inseridos pelo utilizador
     */
    public static void getDataFromUser(String[] validatedParameters) {

        try {
            getStartAndEndDate(validatedParameters, 0);
            clearConsole();
            checkDataValidity(validatedParameters);
            processData(validatedParameters);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    /**
     * @param validatedParameters parâmetros inseridos pelo utilizador, validados
     * @param method
     */
    public static void getStartAndEndDate(String[] validatedParameters, int method) {
        try {
            reader = new Scanner(System.in);

            System.out.println("-------------------------------");
            if (method != 3) {
                System.out.println("      INTRODUÇÃO DE DATAS      ");
            } else {
                System.out.println("      INTRODUÇÃO DE DATA       ");
            }

            if (method == 1) {
                System.out.println("      PRIMEIRO  INTERVALO      ");
            } else {
                if (method == 2) {
                    System.out.println("       SEGUNDO INTERVALO       ");
                }
            }

            if (method != 3) {
                System.out.println("-------------------------------");
                System.out.println("Data de inicio");
                validatedParameters[1] = reader.nextLine();
                System.out.println("Data de fim");
                validatedParameters[2] = reader.nextLine();
            } else {
                System.out.println("-------------------------------");
                System.out.println("Data pretendida");
                validatedParameters[1] = reader.nextLine();
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    /**
     * Método que questiona o utilizador o tipo de dados que pretende tratar e obter
     * se o método for 0 vai solicitar dados relativos à resolução temporal
     * se o modo for 0 está em modo de comparação e não podem ser selecionadas todas
     * as colunas.
     *
     * @param validatedParameters parâmetros inseridos pelo utilizador, validados
     * @param method              método relativo à resolução temporal
     * @param mode
     */
    public static void showGetDataTypeMenu(String[] validatedParameters, int method, int mode) {

        try {
            reader = new Scanner(System.in);
            int option = 0;

            clearConsole();

            do {
                System.out.println("-------------------------------");
                System.out.println("         TIPO DE DADOS         ");
                System.out.println("-------------------------------");

                if (mode == 0) {
                    System.out.println("(1) Todos");
                } else {
                    System.out.println("(1) Opção indisponível");
                }

                System.out.println("(2) Infectados");
                System.out.println("(3) Hospitalizados");
                System.out.println("(4) Internados UCI");
                System.out.println("(5) Mortes");
                System.out.println("(6) Regressar ao Menu Anterior");
                System.out.println("(7) Regressar ao Menu Principal");
                System.out.println("-------------------------------");
                System.out.print("Indique a sua opção: ");

                reader = new Scanner(System.in);

                option = reader.nextInt();

                switch (option) {
                    case 1:
                        if (mode == 0) {

                            clearConsole();
                            validatedParameters[5] = Integer.toString(option);
                            if (method == 0) {
                                getDataFromUser(validatedParameters);
                            } else {
                                option = 7;
                            }
                        }
                        clearConsole();
                        break;
                    case 2:
                        System.out.println("-");
                        System.out.println("Vai tratar todos os infectados");
                        System.out.println("-");
                        pressEnterToContinue();
                        clearConsole();
                        validatedParameters[5] = Integer.toString(option);
                        if (method == 0) {
                            getDataFromUser(validatedParameters);
                        } else {
                            option = 7;
                        }
                        break;
                    case 3:
                        System.out.println("-");
                        System.out.println("Vai tratar todos os hospitalizados");
                        System.out.println("-");
                        pressEnterToContinue();
                        clearConsole();
                        validatedParameters[5] = Integer.toString(option);
                        if (method == 0) {
                            getDataFromUser(validatedParameters);
                        } else {
                            option = 7;
                        }
                        break;
                    case 4:
                        System.out.println("-");
                        System.out.println("Vai tratar todos os internados UCI");
                        System.out.println("-");
                        pressEnterToContinue();
                        clearConsole();
                        validatedParameters[5] = Integer.toString(option);
                        if (method == 0) {
                            getDataFromUser(validatedParameters);
                        } else {
                            option = 7;
                        }
                        break;
                    case 5:
                        System.out.println("-");
                        System.out.println("Vai tratar todas as mortes");
                        System.out.println("-");
                        pressEnterToContinue();
                        clearConsole();
                        validatedParameters[5] = Integer.toString(option);
                        if (method == 0) {
                            getDataFromUser(validatedParameters);
                        } else {
                            option = 7;
                        }
                        break;
                    case 6:
                        clearConsole();
                        if (method == 0) {
                            showGetDataMenu(validatedParameters);
                        } else {
                            callInteractiveMode(validatedParameters);
                        }
                        break;
                    case 7:
                        clearConsole();
                        callInteractiveMode(validatedParameters);
                        break;
                    default:
                        System.out.println("-");
                        System.out.println("Opção inválida!");
                        System.out.println("-");
                        pressEnterToContinue();
                        clearConsole();
                        break;
                }

            } while (option != 7);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Este método apresenta o menu para que o utilizador possa escolher o tipo de ficheiro a carregar
     *
     * @param receivedParameters parâmetros inseridos pelo utilizador
     */
    public static void showFileMenu(String[] receivedParameters) {

        try {
            reader = new Scanner(System.in);
            int option = 0;
            clearConsole();
            do {
                System.out.println("-------------------------------");
                System.out.println("       CARREGAR FICHEIRO       ");
                System.out.println("-------------------------------");
                System.out.println("(1) Ficheiro de Acumulados");
                System.out.println("(2) Ficheiro de Totais");
                System.out.println("(3) Ficheiro de Markov");
                System.out.println("(4) Regressar ao Menu Principal");
                System.out.println("-------------------------------");
                System.out.print("Indique a sua opção: ");

                option = reader.nextInt();

                switch (option) {
                    case 1:
                    case 2:
                    case 3:
                        clearConsole();


                        if (option != 3) {

                            receivedParameters[4] = Integer.toString(option);
                        }


                        getFileNameFromUser(receivedParameters, option);
                        callInteractiveMode(receivedParameters);
                        break;
                    case 4:
                        clearConsole();
                        callInteractiveMode(receivedParameters);
                        break;
                    default:
                        System.out.println("-");
                        System.out.println("Opção inválida!");
                        System.out.println("-");
                        pressEnterToContinue();
                        clearConsole();
                        break;
                }

            } while (option != 4);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Método que obtém o nome do ficheiro que o utilizador quer carregar
     *
     * @param receivedParameters
     * @param option
     */
    public static void getFileNameFromUser(String[] receivedParameters, int option) {
        try {
            reader = new Scanner(System.in);
            System.out.println("Introduza o nome do ficheiro a carregar:");
            if (option == 3) {   // se for um ficheiro de Markov
                receivedParameters[6] = reader.nextLine().trim();
                System.out.println("O nome do ficheiro de Markov foi: '" + receivedParameters[6] + "'");
                if (checkIfFileExists(receivedParameters[6])) {
                    System.out.println("-");
                    System.out.println("O ficheiro existe e vai carregar.");
                    System.out.println("-");
                    pressEnterToContinue();
                    clearConsole();
                } else {
                    System.out.println("-");
                    System.out.println("O ficheiro não existe.");
                    System.out.println("-");
                    receivedParameters[6] = "-";
                    pressEnterToContinue();
                    clearConsole();
                }
            } else { // se for um ficheiro de acumulados ou totais
                receivedParameters[3] = reader.nextLine().trim();
                if (checkIfFileExists(receivedParameters[3])) {
                    System.out.println("-");
                    System.out.println("O ficheiro existe e vai carregar.");
                    System.out.println("-");
                    pressEnterToContinue();
                    clearConsole();
                } else {
                    System.out.println("-");
                    System.out.println("O ficheiro não existe.");
                    System.out.println("-");
                    receivedParameters[4] = null;
                    receivedParameters[3] = null;
                    pressEnterToContinue();
                    clearConsole();
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Verifica se o ficheiro dado como argumento existe ou não e retorna o booleano
     * representativo
     *
     * @param inputFileName nome do ficheiro
     * @return true se existir, false se nao existir
     */
    public static boolean checkIfFileExists(String inputFileName) {

        File userFileName = new File(inputFileName);

        boolean fileExists = false;

        try {

            if (userFileName.exists() && !userFileName.isDirectory()) {
                fileExists = true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return fileExists;
    }

    /**
     * Soma todos os dias do mes para o ficheiro de totais
     *
     * @param data          matriz recebida
     * @param i             coluna a somar
     * @param firstDate     data a iniciar a soma
     * @param lengthOfMonth tamanho do mes
     * @return dias do mês para o ficheiro de totais
     */
    public static String sumTotalMonth(String[][] data, int i, int firstDate, int lengthOfMonth) {
        String result = "0";
        try {
            for (int j = firstDate; j < firstDate + lengthOfMonth; j++) {
                result = Integer.toString(Integer.parseInt(result) + Integer.parseInt(data[j][i]));
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    /**
     * metodo para somar todos os dias da semana quando o ficheiro introduzido é de
     * totais.
     *
     * @param data matriz recebida
     * @param i    numero da primeira linha a somar
     * @param j    coluna a somar
     * @return dias da semana para ficheiro de valores totais
     */
    public static String sumTotalWeek(String[][] data, int i, int j) {
        String result = "0";
        try {
            for (int k = i; k < i + 7; k++) {
                result = Integer.toString(Integer.parseInt(result) + Integer.parseInt(data[k][j]));
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

    /**
     * constrói a matriz de valores para o ficheiro de Markov
     *
     * @param markovFile     ficheiro com os valores de transição de Markov
     * @param typeOfAnalysis tipo de análise: 1 para previsão, 2 para transição de
     *                       estado
     * @return matriz preenchida com o conteúdo do ficheiro
     */
    public static String[][] readMarkovFile(File markovFile, int typeOfAnalysis) {
        String[][] markovTransitionMatrix = null;

        try {
            reader = new Scanner(markovFile);

            int matrixSize = 0;

            if (typeOfAnalysis == 1) {
                matrixSize = 5;
            } else if (typeOfAnalysis == 2) {
                matrixSize = 4;
            } else {

                System.out.println("");
                System.out.println("por favor inserir o valor '1' para matriz de tamanho 5 e '2' para matriz de tamanho 4");
                System.out.println("");

            }

            markovTransitionMatrix = new String[matrixSize][matrixSize];

            int counter = 0;
            int secondCounter = 0;

            while (reader.hasNextLine() && counter < matrixSize) {
                for (int i = 0; (i < markovTransitionMatrix.length); i++) {
                    String line = reader.nextLine();
                    // tem que se eliminar as leituras de linhas em branco
                    if (line.isEmpty()) {
                        i--;

                    } else {
                        String[] values = line.split("=");
                        markovTransitionMatrix[counter][i] = values[1];
                        if (secondCounter == markovTransitionMatrix.length - 1) {
                            counter++;
                            secondCounter = 0;
                        } else {
                            secondCounter++;
                        }
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            reader.close();

        }

        return markovTransitionMatrix;
    }

    // ---------------------------- Metodo comparação ------------------------------------------------------------------

    /**
     * método responsavel para executar a comparacao diaria de 2 intervalos de datas
     * o número de datas será sempre o mais curto dos 2 intervalos
     * no fim retorna uma matriz pronta a exibir
     *
     * @param comparisonParameters parametros introduzidos pelo utilizador
     * @return
     */
    public static String[][] comparisonMode(String[] comparisonParameters) {

        String[][] dataToShow = null;
        try {
            int dayAndDayBefore = 2;

            // esta matriz vazia retorna quando nenhuma das opções não são escolhidas, como
            // as opçoes sao todas validas nunca chega a retornar esta matriz

            String fileName = comparisonParameters[6];
            File file = new File(fileName);

            // data de inicio de de fim em formato normal para usar em ficheiro de totais
            String startDateString1 = comparisonParameters[0];
            String endDateString1 = comparisonParameters[1];
            String startDateString2 = comparisonParameters[2];
            String endDateString2 = comparisonParameters[3];

            // data de inicio e de fim invertidas para usar no ficheiro de acumulados
            LocalDate startDate1 = invertDateFormat(startDateString1);
            LocalDate endDate1 = invertDateFormat(endDateString1);
            LocalDate startDate2 = invertDateFormat(startDateString2);
            LocalDate endDate2 = invertDateFormat(endDateString2);

            switch (comparisonParameters[4]) {
                case "1":
                    startDateString1 = String.valueOf(startDate1);
                    endDateString1 = String.valueOf(endDate1);
                    startDateString2 = String.valueOf(startDate2);
                    endDateString2 = String.valueOf(endDate2);
                    break;
                case "2":
                    startDateString1 = startDateString1;
                    startDateString2 = startDateString2;
                    break;

                default:
                    System.out.println("-");
                    System.out.println("Opção inválida.");
                    System.out.println("-");
            }

            if (validateDateInFile(startDateString1, file) && validateDateInFile(endDateString1, file)
                    && validateDateInFile(startDateString2, file) && validateDateInFile(endDateString2, file)) {
                // calcula o numero de dias dentro do intervalo para dimensionar matriz
                int daysDate1 = countDays(startDate1, endDate1) + dayAndDayBefore;
                int daysDate2 = countDays(startDate2, endDate2) + dayAndDayBefore;

                // matriz com os valores dentro do intervalo para acumulados e totais
                String[][] data1 = readData(file, daysDate1, startDate1, Integer.parseInt(comparisonParameters[4]));
                String[][] data2 = readData(file, daysDate2, startDate2, Integer.parseInt(comparisonParameters[4]));

                // inicializa 2 arrays para utilizar dentro dos if
                String[][] dataClean1 = new String[daysDate1][2];
                String[][] dataClean2 = new String[daysDate2][2];

                // transforma a matriz do intervalo orifinal numa matriz de apenas 2 colunas,
                // (data e coluna escolhida)
                if (comparisonParameters[6].equalsIgnoreCase("2")) {
                    dataClean1 = matrixTransformationForComparison(data1, Integer.parseInt(comparisonParameters[5]),
                            daysDate1);
                    dataClean2 = matrixTransformationForComparison(data2, Integer.parseInt(comparisonParameters[5]),
                            daysDate2);
                } else {
                    dataClean1 = getDailyData(data1, daysDate1, comparisonParameters);
                    dataClean2 = getDailyData(data2, daysDate2, comparisonParameters);

                }

                // inicializa a matriz para exibir que vai juntar as 2 matrizes criadas
                // anteriormente com 2 colunas e
                // adiciona mais 1 coluna para mais a frente colocar a diferença entre valores
                // no final fica uma matriz de 5 colunas com o numero de linhas dependente do
                // menor intervalo
                dataToShow = organizeMatrixToShow(dataClean1, dataClean2, daysDate1, daysDate2);

                // calcula a diferenca para adicionar na ultima coluna
                calculateDifference(dataToShow, daysDate1, daysDate2);

            } else {
                comparisonParameters[3] = null;
                String[][] dataToShowIfFail = {
                        {"------------------------------------------------------------------"},
                        {"Não é possivel fazer comparações, por favor introduza novas datas."},
                        {"------------------------------------------------------------------"}
                };

                showData(dataToShowIfFail);
                pressEnterToContinue();
                callInteractiveMode(comparisonParameters);

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return dataToShow;
    }

    public static void mergeTwoMatrixInOne(String[][] dataToShow, String[][] avgAndStdDeviation,
                                           String[][] dataToShowWithMediaAndStd) {
        int firstLine = 0;
        for (int i = 0; i < dataToShow.length; i++) {
            for (int j = 0; j < dataToShow[0].length; j++) {
                dataToShowWithMediaAndStd[i][j] = dataToShow[i][j];
            }
        }
        // este ciclo preenche 1 linha vazia para separar a ultima data da media
        for (int i = 0; i < dataToShowWithMediaAndStd[0].length; i++) {
            dataToShowWithMediaAndStd[dataToShow.length][i] = "";
        }
        for (int i = dataToShow.length + 1; i < dataToShowWithMediaAndStd.length; i++) { // "+1" par deixar 1 linha vaga
            for (int j = 0; j < dataToShowWithMediaAndStd[0].length; j++) {
                dataToShowWithMediaAndStd[i][j] = avgAndStdDeviation[firstLine][j];

            }
            firstLine++;
        }
    }

    /**
     * este metodo apenas serve para calcular a diferenca entre os valores das
     * colunas no modo de comparação
     *
     * @param dataToShow dados a tratar
     * @param daysDate1  dias da matriz 1 a considerar
     * @param daysDate2  dias da matriz 2 a considerar
     */
    public static void calculateDifference(String[][] dataToShow, int daysDate1, int daysDate2) {
        try {
            int daysToConsider = 0;
            if (daysDate1 >= daysDate2) {
                daysToConsider = daysDate2;
            } else {
                daysToConsider = daysDate1;
            }
            for (int i = 1; i < daysToConsider; i++) {
                if (Integer.parseInt(dataToShow[i][1]) > Integer.parseInt(dataToShow[i][3])) {
                    dataToShow[i][4] = Integer
                            .toString(Integer.parseInt(dataToShow[i][1]) - Integer.parseInt(dataToShow[i][3]));
                } else if (Integer.parseInt(dataToShow[i][1]) < Integer.parseInt(dataToShow[i][3])) {
                    dataToShow[i][4] = Integer
                            .toString(Integer.parseInt(dataToShow[i][3]) - Integer.parseInt(dataToShow[i][1]));
                } else {
                    dataToShow[i][4] = "0";
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * recebe as 2 matrizes limpas e junta tudo numa só,
     * com a coluna das diferencas vazia para calcular mais a frente
     *
     * @param dataClean1 matriz limpa 1
     * @param dataClean2 matriz limpa 2
     * @param daysDate1  dias da matriz 1
     * @param daysDate2  dias da matriz 2
     * @return matriz organizada
     */
    public static String[][] organizeMatrixToShow(String[][] dataClean1, String[][] dataClean2, int daysDate1,
                                                  int daysDate2) {
        String[][] result = null;
        try {
            int daysToConsider = 0;
            if (daysDate1 >= daysDate2) {
                daysToConsider = daysDate2;
            } else {
                daysToConsider = daysDate1;
            }
            result = new String[daysToConsider][5];
            result[0][4] = "Diferença";
            for (int i = 0; i < daysToConsider; i++) {
                for (int j = 0; j < 2; j++) {
                    result[i][j] = dataClean1[i][j];
                    // vai completar a 1 e 2 coluna com a primeira matriz "limpa"
                    result[i][j + 2] = dataClean2[i][j];
                    // +2 significa que vai completar a 3 e 4 coluna com a segunda matriz "limpa"
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

    /**
     * transforma os dados totais recebidos numa matriz de apenas 2 colunas
     * data e coluna escolhida
     *
     * @param data         matriz a calcular
     * @param columnToShow numero da coluna a mostrar
     * @param days         numero de dias
     * @return resultado final
     */
    public static String[][] matrixTransformationForComparison(String[][] data, int columnToShow, int days) {
        String[][] result = new String[days][2];
        try {
            int column;
            if (data[0].length > 5) {
                column = columnToShow;
            } else {
                column = columnToShow - 1;
                // o "-1" é porque o metodo de calculo de acumulados nao retorna a coluna dos nao infectados
                // dai termos de reduzir 1 posicao as colunas
            }
            // preenche o cabeçalho da matriz
            fillFirstLineForComparisonMatrix(result, columnToShow);
            for (int i = 1; i < days; i++) {
                result[i][0] = data[i][0];
                result[i][1] = data[i][column];
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

    /**
     * preenche o cabeçalho da matriz para comparacao, coloca data e nome da coluna
     *
     * @param result       matriz resultada com o resultado pretendido
     * @param columnToShow nome coluna a ser preenchida
     */
    public static void fillFirstLineForComparisonMatrix(String[][] result, int columnToShow) {
        try {
            result[0][0] = "Data";
            switch (columnToShow) {
                case 2:
                    result[0][1] = "Infectado";
                    break;
                case 3:
                    result[0][1] = "Hospitalizado";
                    break;
                case 4:
                    result[0][1] = "Internado UCI";
                    break;
                case 5:
                    result[0][1] = "Mortes";
                    break;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Método para calcular a média dos dados existentes no array, nas posições dos
     * valores
     *
     * @param data matriz de dados
     * @return matriz de média
     */
    public static String[][] calculateAverage(String[][] data) {
        String avg[][] = new String[2][5];

        try {
            double initialDateAverage = 0.0;
            double finalDateAverage = 0.0;
            double diferenceAverage = 0.0;

            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data[i].length; j++) {

                    if (i != 0) {
                        if (j == 1)
                            initialDateAverage += Double.parseDouble(data[i][j]);
                        if (j == 3)
                            finalDateAverage += Double.parseDouble(data[i][j]);
                        if (j == 4)
                            diferenceAverage += Double.parseDouble(data[i][j]);
                    }
                }
            }

            avg[0][0] = "Media";
            avg[0][1] = String.format(Locale.ROOT, "%.4f", initialDateAverage /= (data.length - 1));
            avg[0][2] = "";
            avg[0][3] = String.format(Locale.ROOT, "%.4f", finalDateAverage /= (data.length - 1));
            avg[0][4] = String.format(Locale.ROOT, "%.4f", diferenceAverage /= (data.length - 1));
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return avg;
    }

    /**
     * Método que efectua o cálculo do desvio padrão, recebendo a matriz de dados
     * completa e a matriz
     * da média onde irá guardar também o desvio padrão
     *
     * @param data         matriz de dados
     * @param stdDeviation desvio padrão
     * @return matriz com desvio padrão
     */
    public static String[][] calculateStdDeviation(String[][] data, String[][] stdDeviation) {

        try {
            double initialDateAverage = 0.0;
            double finalDateAverage = 0.0;
            double diferenceAverage = 0.0;
            double stdDeviationInitialDate = 0.0;
            double stdDeviationFinalDate = 0.0;
            double stdDeviationDiference = 0.0;

            initialDateAverage = Double.parseDouble(stdDeviation[0][1]);
            finalDateAverage = Double.parseDouble(stdDeviation[0][3]);
            diferenceAverage = Double.parseDouble(stdDeviation[0][4]);

            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data[i].length; j++) {
                    if (i != 0) {
                        if (j == 1)
                            stdDeviationInitialDate += Math.pow(Double.parseDouble(data[i][j]) - initialDateAverage, 2);
                        if (j == 3)
                            stdDeviationFinalDate += Math.pow(Double.parseDouble(data[i][j]) - finalDateAverage, 2);
                        if (j == 4)
                            stdDeviationDiference += Math.pow(Double.parseDouble(data[i][j]) - diferenceAverage, 2);

                    }
                }
            }

            stdDeviation[1][0] = "Desvio Padrão";
            stdDeviation[1][1] = String.format("%.4f", Math.sqrt(stdDeviationInitialDate / (data.length - 1)));
            stdDeviation[1][2] = "";
            stdDeviation[1][3] = String.format("%.4f", Math.sqrt(stdDeviationFinalDate / (data.length - 1)));
            stdDeviation[1][4] = String.format("%.4f", Math.sqrt(stdDeviationDiference / (data.length - 1)));
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return stdDeviation;

    }

    // ------------------------- Multiplicar matrizes ---------------------------------------------

    /**
     * usado para fazer a transposição de uma matriz
     *
     * @param matrix matriz enviada para ser transposta
     * @return matriz transposta
     */
    public static String[][] transposeMatrix(String[][] matrix) {

        String[][] transposedMatrix = new String[countColumns(matrix)][1];

        try {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    transposedMatrix[j][i] = matrix[i][j];
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return transposedMatrix;
    }

    /**
     * @param matrix matriz enviada para ser contado o número de colunas
     * @return número de colunas da matriz
     */
    public static int countColumns(String[][] matrix) {
        int columns = 0;

        try {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    columns++;
                }
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return columns;
    }

    /**
     * @param markovData        matriz com os dados do ficheiro das transições de markov
     * @param dataToBeEvaluated matriz com dados para serem aplicados os valores das transições de markov
     * @return valores das previsões de markov
     */
    public static String[][] calculateMarkovMatrixAplication(String[][] markovData, String[][] dataToBeEvaluated) {
        String[][] transposedDataToBeEvaluated = transposeMatrix(dataToBeEvaluated);
        double[][] auxMarkovCalculation = new double[5][1];
        String[][] markovMatrixForesight = new String[5][1];

        try {
            for (int i = 0; i < transposedDataToBeEvaluated.length; i++) {
                for (int j = 0; j < markovData.length; j++) {
                    if (j == 0) {
                        auxMarkovCalculation[i][0] = (Double.parseDouble(markovData[i][j]))
                                * Double.parseDouble(transposedDataToBeEvaluated[j][0]);
                    } else {
                        auxMarkovCalculation[i][0] = auxMarkovCalculation[i][0] + (Double.parseDouble(markovData[i][j]))
                                * (Double.parseDouble(transposedDataToBeEvaluated[j][0]));
                    }
                }
                markovMatrixForesight[i][0] = String.format("%.1f", auxMarkovCalculation[i][0]);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return markovMatrixForesight;
    }

    /**
     * Método que cria a matrix identidade para ser utilizada na aplicação do método
     * de Crout
     *
     * @return matriz quadrada 4x4 com diagonal a 1.0
     */
    public static String[][] createIdentityMatrix() {

        String[][] matrix = new String[sizeMatrix][sizeMatrix];

        try {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    if (i == j) {
                        matrix[i][j] = "1";
                    } else {
                        matrix[i][j] = "0";
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return matrix;
    }

    /**
     * @param markovData matriz com os valores de transição de markov
     * @return cópia da matriz com os valores de markov
     */
    public static String[][] copyMarkovMatrix(String[][] markovData) {

        int finalMatrixSize = 4;
        String[][] newMarkovDataWithoutDeaths = new String[finalMatrixSize][finalMatrixSize];

        try {
            String[][] cutRowFromMatrix = Arrays.copyOf(markovData, markovData.length - 1);

            for (int i = 0; i < newMarkovDataWithoutDeaths.length; i++) {
                for (int j = 0; j < newMarkovDataWithoutDeaths[i].length; j++) {
                    newMarkovDataWithoutDeaths[i][j] = cutRowFromMatrix[i][j];
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return newMarkovDataWithoutDeaths;
    }

    /**
     * Método que calcula a matriz inferior, uma matriz que a partir de uma matriz
     * quadrada original apresenta os valores inferiores à diagonal (inclusive)
     * Deve receber a matriz do metodo de crout para estudo da transição de estados
     *
     * @param markovFile recebe o ficheiro de Markov introduzido pelo utilizador
     * @return a matrix inferior
     */
    public static String[][] matrixLower(File markovFile) {
        String[][] matrixLower = createIdentityMatrix();

        try {
            String[][] matrixMarkov = readMarkovFile(markovFile, 1);

            for (int i = 0; i < matrixLower.length; i++) {
                for (int j = 0; j < matrixLower[i].length; j++) {
                    if (i >= j) {
                        matrixLower[i][j] = matrixMarkov[i][j];
                    } else {
                        matrixLower[i][j] = "0";
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return matrixLower;
    }

    /**
     * Método que calcula a matriz superior, uma matriz que a partir de uma matriz
     * quadrada original apresenta apenas os valores superiores à diagonal
     * Deve receber a matriz do metodo de crout para estudo da transição de estados
     *
     * @param markovFile recebe o ficheiro de Markov introduzido pelo utilizador
     * @return a matrix superior
     */
    public static String[][] matrixUpper(File markovFile) {

        String[][] matrixUpper = createIdentityMatrix();

        try {
            String[][] matrixMarkov = readMarkovFile(markovFile, 2);

            for (int i = 0; i < matrixUpper.length; i++) {
                for (int j = 0; j < matrixUpper[i].length; j++) {
                    if (i < j)
                        matrixUpper[i][j] = matrixMarkov[i][j];
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return matrixUpper;
    }

    /**
     * Este método cria faz a multiplicação das matrizes inferior e superior e gera
     * a nova matriz LU
     *
     * @param lowerMatrix matriz 4x4 inferior
     * @param upperMatrix matriz 4x4 superior
     * @return uma matriz 4x4 com o resultado da muliplicação da matriz inferior com
     * a matriz superior
     */
    public static String[][] multiplicationMatrix(String[][] lowerMatrix, String[][] upperMatrix) {

        String[][] result = new String[lowerMatrix.length][upperMatrix[0].length];

        try {
            // assim conseguimos ir buscar as linhas de uma matriz e as colunas da outra
            // para conseguirmos calcular nao so matrizes quadradas como também matrizes 3x2
            // x 2x3
            for (int i = 0; i < lowerMatrix.length; i++) {
                for (int j = 0; j < lowerMatrix[0].length; j++) {
                    int intermediateCalculation = 0;
                    for (int k = 0; k < lowerMatrix[0].length; k++) {
                        intermediateCalculation += (Double.parseDouble(lowerMatrix[i][k])
                                * Double.parseDouble(upperMatrix[k][j]));
                        result[i][j] = String.valueOf(intermediateCalculation);
                    }
                }

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

}
